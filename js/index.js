$(document).ready(function(){

    $(".menu-toggle-left").click(function(){
        if($("#mobile-nav-left").hasClass("onleft")){
            $("#mobile-nav-left").removeClass("onleft");
            $(".container").removeClass("onleft");
            $("body").removeClass("onleft");
        }else{
            $("#mobile-nav-left").addClass("onleft");
            $(".container").addClass("onleft");
            $("body").addClass("onleft");
        }
    });

    $(".menu-toggle-right").click(function(){
        if($("#mobile-nav-right").hasClass("onright")){
            $("#mobile-nav-right").removeClass("onright");
            $(".container").removeClass("onright");
            $("body").removeClass("onright");
        }else{
            $("#mobile-nav-right").addClass("onright");
            $(".container").addClass("onright");
            $("body").addClass("onright");
        }
    });

});    