var gedb = {
    
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        var getdb = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
    },
    // Bind Event Listeners
    bindEvents: function() {
        document.addEventListener('deviceready',this.onDeviceReady(), false);
    },
    // deviceready Event Handler
    onDeviceReady: function(id) {
        this.dbGEOpen('deviceready');
    },
    // Create DB
    dbGEOpen: function(id) {
         var getdb = window.openDatabase('GEDB', '1.0', 'Test DB', 25242880);
          getdb.transaction(this.runTransaction, this.errorlog, this.success);
    },
    //Execute transaction
    runTransaction: function(t){
        t.executeSql('CREATE TABLE IF NOT EXISTS geDatas (id unique, gestrings)');
    },
    //Log error
    errorlog: function(err){
         console.log('Error creating tables: '+err);
    },
    // Success load
    success: function(){
        console.log('Successfully created tables');
    },
    // Insert Data
    saveTransaction: function(){
        var getdb = window.openDatabase('GEDB', '1.0', 'Test DB', 25242880);
        getdb.transaction(this.insertData, this.saveError, this.saveSuccess);
    },
    insertData: function(t){
        var valID = $("#name").val();
        t.executeSql("INSERT INTO geDatas (gestrings) VALUES ('"+valID+"')");
        $("#name").val("");
    },
    saveError: function(err){
        console.log('Error: '+err);
    },
    // Success message on save
    saveSuccess: function(){
         console.log('Successfully added');
    },
    loadGEData: function(){
        var getdb = window.openDatabase('GEDB', '1.0', 'Test DB', 25242880);
        getdb.transaction(this.runSelect, this.saveError);
    },
    runSelect: function(t){
        t.executeSql('SELECT * FROM geDatas', [], gedb.successSelect, this.saveError);
    },  
    successSelect: function(t, results){
        var len = results.rows.length;
        var data = '';
        for(var i = 0; i < len; i++){
            data += results.rows.item(i).gestrings+", ";
        }
        $(".dataStrings").text("DATA: "+data);
    }

};  