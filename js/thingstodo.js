function reloadPage(){
	var connection = checkConnection();
    if(connection == 'none'){
    	readCoursesJsonlocal();
    }
    else{
    	getCourse();
    }
}

function checkConnection() {
    var networkState = navigator.network.connection.type;
    return networkState;
}

function openCoursetoDB(){
	var getdb = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
	getdb.transaction(transactionExe, error, successCreation);
}

function transactionExe(t){
	t.executeSql('CREATE TABLE IF NOT EXISTS courses (id unique, downloaded)');

			var api_link = 'http://www.miyens.com/gep';

			$.ajax({
				 type: "POST",
				 url: api_link+"/userapi/courses/",
				 dataType: "json"
			})
			.done(function( result ) {

				$.each(result, function(index, data){

					var getdb = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
					getdb.transaction(function(t){

						t.executeSql('SELECT * FROM courses WHERE id=?', [data.id], function (t, datas){
							if(datas.rows.length == 0){
								$(".courses .btn-start[data-course-id='"+data.id+"'] ").text("Download");
								$(".courses .btn-start[data-course-id='"+data.id+"'] ").removeClass("btn-start").addClass("btn-download");	
								console.log("Download");			
							}
						});

					}, 
					error, 
					successCreation);

					

				});

			})
			.fail(function( error, status ) {
			    console.log(error);
			});
}

function successCreation(){
	console.log('Successfully created tables');
}

function getCourse(){
			var link = 'http://www.miyens.com/gep';

			$.ajax({
				 type: "POST",
				 url: link+"/userapi/courses/",
				 dataType: "json"
			})
			.done(function( result ) {
			 	 if(result != ''){
			 	 	var fileObject;
					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem){

                        fileSystem.root.getDirectory("theacademy",{create:true},function(d){
                            fileSystem.root.getFile("theacademy/info-device.txt", {create: true}, function(fileEntry){
                                fileObject = fileEntry;

                                fileObject.createWriter(function gotfileWriter(writer){

                                	writer.write(result);

                                }, error);

                                readJson(fileObject);
                            }, 
                            error);
                        },
                        error);

                    },
                    error);
			 	 }
			 	 else{
			 	 	$('.myModal .modal-content p').text("No Courses Available!");
					$('.myModal').modal();
			 	 }
			})
			.fail(function( error, status ) {
			    alert('failed');
			});
}

function readJson(fileEntry){

                                fileEntry.file(function fileEntryJson(file){

                                		console.log("called the file func on the file ob");

						                var reader = new FileReader();
						                reader.onloadend = function(evt) {
						                    console.log("successfully read text");
						                    console.log("Target Result ["+evt.target.result+"]");
						                    console.log("Reader Result ["+reader.result+"]");
						                    var result = $.parseJSON(reader.result);
						                    $.each(result, function(i, data){
			 	 			

									 	 		    var html = '<div class="row bothBorder">';
												    html += 	'<div class="col-xs-2 css-right-nav">';
												    html += 		'<img src="img/'+data.icon+'.png" >';
												    html += 	'</div>';
												    html += 	'<div class="col-xs-6 css-left-nav">';
												    html += 		'<p class="challenges">'+data.title+'</p>';
												    html += 	'</div>';
												    html += 	'<div class="col-xs-4 css-right-nav">';
												    html += 		'<button class="btn-start" data-course-id="'+data.id+'">Start</button>';
												    html += 	'</div>';
												    html += 	'<div class="col-xs-12 css-left-nav">';
												    html += 		'<p class="challenges">'+data.description+'</p>';
												    html += 	'</div>';												    html += '</div>';

												    $(".courses").append(html);

									 	 	});
						                };

						                console.log("Reading text");
						                reader.readAsText(file);

                                }, error);

								openCoursetoDB();
                            
}

function readCoursesJsonlocal(){
					window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem){

                        fileSystem.root.getDirectory("theacademy",{create:true},function(d){
                            fileSystem.root.getFile("theacademy/info-device.txt", {create: false}, function(fileEntry){
                                fileEntry.file(function fileEntryJson(file){

                                		console.log("called the file func on the file ob");

						                var reader = new FileReader();
						                reader.onloadend = function(evt) {
						                    console.log("successfully read text");
						                    console.log("Target Result ["+evt.target.result+"]");
						                    console.log("Reader Result ["+reader.result+"]");
						                    var result = $.parseJSON(reader.result);
						                    var selectors
						                    $.each(result, function(i, data){
			 	 			
									 	 		    var html = '<div class="row bothBorder">';
												    html += 	'<div class="col-xs-2 css-right-nav">';
												    html += 		'<img src="img/'+data.icon+'.png" >';
												    html += 	'</div>';
												    html += 	'<div class="col-xs-6 css-left-nav">';
												    html += 		'<p class="challenges">'+data.title+'</p>';
												    html += 	'</div>';
												    html += 	'<div class="col-xs-4 css-right-nav">';
												    html += 		'<button class="btn-start" data-course-id="'+data.id+'" id="data-'+data.id+'">Start</button>';
												    html += 	'</div>';
												    html += 	'<div class="col-xs-12 css-left-nav">';
												    html += 		'<p class="challenges">'+data.description+'</p>';
												    html += 	'</div>';
												    html += '</div>';

												    $(".courses").append(html);


												    var getdb = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
													getdb.transaction(function(t){

														t.executeSql('SELECT * FROM courses WHERE id=?', [data.id], function (t, datas){
															if(datas.rows.length == 0){	
																changeText(data.id);
															}
														});

													}, 
													error, 
													successCreation);
									 	 	});
						                };

						                console.log("Reading text");
						                reader.readAsText(file);

                                }, error);
                            }, 
                            error);
                        },
                        error);

                    },
                    error);
					
}
function changeText(id){
	$('#data-'+id+'').text("Download");
}

function error(error){
	 			$('.myModal .modal-content p').text("Error "+error.code);
				$('.myModal').modal();
}

function downloadFiles(id, token){
			var api_link = 'http://www.miyens.com/gep';

			$.ajax({
				 type: "POST",
				 url: api_link+"/userapi/courses/2/",
				 data: {token: token},
				 dataType: "json"
			})
			.done(function( result ) {

				$.each(result.elements, function(index, data){
					var filePath = data.url;
					var filename = filePath.split("/").pop();
					
					downloadFile(filename, filePath);
				});

			})
			.fail(function( error, status ) {
			    console.log(error);
			});
}

function downloadFile(filename,fileurl){
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem){

                        fileSystem.root.getDirectory("theacademy",{create:true},function(d){
                            fileSystem.root.getFile(filename, {create: true}, function(fileEntry){
                                var fileTransfer = new FileTransfer();
                                var uri = encodeURI(fileurl);
                                var filePath = fileEntry.toURL();
                                fileTransfer.download(uri, filePath, function(entry) {
                                        console.log("download complete: " + filePath);
                                    },
                                    function(error) {
                                        console.log("download error source 2 " + error.source);
                                        console.log("download error target 2 " + error.target);
                                        console.log("upload error code 2 " + error.code);
                                    },
                                    false,
                                    {
                                        headers: {
                                            "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                                        }
                                    }
                                );
                            }, 
                            function error(err){
                                console.log("err "+err.code);
                            });
                        },
                        function error(error){
                            alert("Erro "+error.code);
                        });

                    },
                    function error(error){
                        alert(error.code);
                    });
}