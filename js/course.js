var db;

function GetQueryStringParams(sParam){
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('&');
            for (var i = 0; i < sURLVariables.length; i++) 
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) 
                {
                    return sParameterName[1];
                }
            }
}

function error(error){
                $('.myModal .modal-content p').text("Error "+error.message);
                $('.myModal').modal();
}

function error2(error){
                $('.myModal .modal-content p').text("Error "+error.message);
                $('.myModal').modal();
}

// Get Courses
function getCourse(){
    db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
    db.transaction(getCourseData, error);
}

function getCourseData(tx){
    var id   = GetQueryStringParams('id');
    var sql2 = "SELECT * FROM courses WHERE id='"+id+"' ";
    tx.executeSql(sql2, [], getCourse_success);
}

function getCourse_success(tx, results) {
    var len = results.rows.length;
    for (var i=0; i<len; i++) {
        var course = results.rows.item(i);
        $(".course-title h5").append('<img src="img/'+course.icon+'.png" width="20px"> '+course.title+' ' );
        $(".course-description p").append(course.description);
        
        console.log(course.title);
    }

    getCourseDataFiles();
}

// FILES
function getCourseDataFiles(){
    // db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
    db.transaction(getCourseFiles, error);
}

function getCourseFiles(tx){
    var id   = GetQueryStringParams('id');
    var sql = "SELECT * FROM files WHERE fileid="+id;
    tx.executeSql(sql, [], getCourseFilesSuccess);
}

function getCourseFilesSuccess(tx, results) {
    var len = results.rows.length;
    for (var i=0; i<len; i++) {
        var files = results.rows.item(i);
        var urlsystem = window.localStorage.getItem("filesystem");
        var device = checkDevice();
        var url = urlsystem+files.url;

        var html = '<div class="row bothBorder">';
        html +=     '<div class="col-xs-2 css-right-nav">';
        html +=         '<img src="img/material.png" >';
        html +=     '</div>';
        html +=     '<div class="col-xs-6 css-left-nav">';
        html +=         '<p class="challenges">'+files.title+'</p>';
        html +=         '<p class="challenges">Type: '+files.type+'</p>';
        html +=     '</div>';
        html +=     '<div class="col-xs-4 css-right-nav">';
        if(files.type == 'video'){
        html +=         '<button class="btn-start video" data-course-id="'+files.id+'" data-course-file-url="'+files.url+'"> Play </button>';
        html +=         '<video width="100%" id="myvideo'+files.id+'" controls style="display:none;"><source src="'+url+'" type="video/mp4">Your browser does not support HTML5 video.</video>';
        }
        else if(files.type == 'image'){
        html +=         '<button class="btn-start image" data-course-id="'+files.id+'" data-course-file-url="'+files.url+'">View</button>';
        }
        else if(files.type == 'pdf'){
        html +=         '<button class="btn-start pdf" data-course-id="'+files.id+'" data-course-file-url="'+files.url+'">View</button>';
        }
        else if(files.type == 'download'){
        html +=         '<button class="btn-start download" data-course-id="'+files.id+'" data-course-file-url="'+files.url+'">View</button>';
        }
        else if(files.type == 'html'){
        html +=         '<button class="btn-start html" data-course-id="'+files.id+'" data-course-file-url="'+files.url+'">Read</button>';
        }
        else if(files.type == 'link'){
        html +=         '<button class="btn-start link" data-course-id="'+files.id+'" data-course-file-url="'+files.url+'">Open</button>';
        }
        else{
        html +=         '<button class="btn-start" data-course-id="'+files.id+'" data-course-file-url="'+files.url+'">Show</button>';   
        }
        
        html +=     '</div>';
        html += '</div>';

        $(".courses").append(html);
    }
}

function checkDevice(){
    var deviceType = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

    return deviceType;
}

// Function delete databases
function deleteDB(){
    db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
    db.transaction(deleteDBs, error);
}

function deleteDBs(tx){
    var sql2 = "DROP TABLE IF EXISTS courses";
    var sql3 = "DROP TABLE IF EXISTS files";
    tx.executeSql(sql2);
    tx.executeSql(sql3);

    window.location = 'index.html';
}