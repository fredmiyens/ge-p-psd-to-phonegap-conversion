function login(){
	$(document).ready(function(){
		// url of the api
		var url = 'http://www.test.growthacademy.co.uk';
		// login details
		var username = $('input[name="username"]').val();
		var password = $('input[name="password"]').val();
		//check if login fields are not empty
		if(username != '' && password != '' ){
			//process the actual request
			$.ajax({
				 type: "POST",
				 url: url+"/userapi/authenticate/",
				 data: { json: JSON.stringify( { username: username, password: password }) },
				 dataType: "json"
			})
			 .done(function( result ) {
			 	if(result.status == 'success' && result.token != ''){
			 		window.localStorage.setItem("token", result.token);
			 		window.localStorage.setItem("firstname", result.firstname);
			 		window.localStorage.setItem("lastname", result.lastname);
			 		window.localStorage.setItem("image", result.image);
			 		window.location = "thingstodo.html";
			 	}
			 	else{
			 		$('.myModal .modal-content p').text("Please check login details!");
					$('.myModal').modal();
			 	}
			 })
			 .fail(function( error, status ) {
			    $('.myModal .modal-content p').text("Please check login details!");
				$('.myModal').modal();
			  });
		}
		else{
			$('.myModal .modal-content p').text("Please check login details!");
			$('.myModal').modal();
		}
	});
}

function checkToken(){
	var value = window.localStorage.getItem("token");
	if(value != null){
		window.location = "thingstodo.html";
	}
}