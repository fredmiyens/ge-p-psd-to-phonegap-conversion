var sql;
var downloadedCourse;
var downloadedID;
var db;
var dbCreated = false;
var filemap = window.localStorage.getItem("devicefilesystem");
var filecount;
var fileloads = [];
var filedownloaded = 0;

function reloadPage(){ 
	$("#circularG").show();
	var connection = checkConnection();
    if(connection == 'none'){
    	runList();
    }
    else{
    	getCourse();
    }
}

function checkConnection() {
    var networkState = navigator.network.connection.type;
    return networkState;
}

function getCourse(){
			var token = window.localStorage.getItem("token");

			$.ajax({
				 type: "POST",
				 url: site_url+"/userapi/courses/",
				 data: { token: token },
				 dataType: "json"
			})
			.done(function( result ) {
			 	 if(result){
			 	 	sql = result;
			 	 	openCoursetoDB();
			 	 }
			 	 else{
			 	 	$('.myModal .modal-content p').text("No Courses Available!");
					$('.myModal').modal(); 
			 	 }
			})
			.fail(function( error, status ) {
			    error2('failed');
			});
}

function openCoursetoDB(){
	db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
	db.transaction(transactionExe, error, successCreationDb);
}

function transactionExe(t){
		t.executeSql('CREATE TABLE IF NOT EXISTS courses (id unique, icon, title, description, downloaded)');
		$.each(sql, function(i, data){
//		    t.executeSql("INSERT OR IGNORE INTO courses (id, icon, title, description, downloaded) VALUES ("+data.id+", '"+data.icon+"', '"+data.title+"', '"+data.description+"', 'NO') ");
		    t.executeSql("INSERT OR IGNORE INTO courses (id, icon, title, description, downloaded) VALUES (?, ?, ?, ?, ?) ",
		    	[data.id, data.icon, data.title, data.description, 'NO']);
		});

		runList();
}

function successCreationDb(){
	dbCreated = true;
	console.log('Successfully created tables');
}

function error(error){
				console.log("Error "+error.message);
//	 			$('.myModal .modal-content p').text("Error "+error.message);
//				$('.myModal').modal();
}

function error2(error){
				console.log("Error "+error.message);
//	 			$('.myModal .modal-content p').text(error);
//				$('.myModal').modal();
}

function runList(){
	db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
	db.transaction(getCourses, error);
}

function getCourses(tx){
	var sql2 = "SELECT * FROM courses";
	tx.executeSql(sql2, [], getCourses_success);
}

function getCourses_success(tx, results) {
    var len = results.rows.length;
    for (var i=0; i<len; i++) {
    	var course = results.rows.item(i);
    	var html = '<div class="row bothBorder">';
		html += 	'<div class="col-xs-2 css-right-nav">';
		html += 		'<img src="img/'+course.icon+'.png" >';
		html += 	'</div>';
		html += 	'<div class="col-xs-6 css-left-nav">';
		html += 		'<p class="challenges">'+course.title+'</p>';
		html += 	'</div>';
		html += 	'<div class="col-xs-4 css-right-nav">';
		if(course.downloaded == "NO"){
		html += 		'<button class="btn-download" data-course-id="'+course.id+'">Download</button>';
		}
		else{
		html += 		'<button class="btn-start" data-course-id="'+course.id+'">Start</button>';
		}
		html += 	'</div>';
		html += 	'<div class="col-xs-12 css-left-nav">';
		html += 		'<p class="challenges">'+course.description+'</p>';
		html += 	'</div>';
		html += '</div>';

		$(".courses").append(html);

    }
	$("#circularG").hide();
}

function downloadCourse(id){
			var url = site_url+"/userapi/courses/"+id+"/";
			//var url = dev_site_url+"/userapi/courses/2/";
			var token = window.localStorage.getItem("token");
//			$('.btn-download').prop('disabled', true);
			$.ajax({
				 type: "POST",
				 url: url,
				 dataType: "json"
			})
			.done(function( result ) {
				if(result != ''){
					downloadedCourse = result.elements;
					downloadedID = result.id;


					$("button[data-course-id="+downloadedID+"]").text("Loading...");
                    $("button[data-course-id="+downloadedID+"]").prop('disabled', true);

					filecount = result.elements.length;
					fileloads = [];
					filedownloaded = 0;
					$.each(result.elements, function(i, data){
						var filePath = data.url;
						var filename = filePath.split("/").pop();
						if(data.type != 'link'){
							downloadFile(filename, filePath, i);
						}
						else{
							fileloads[i]=100;
							filedownloaded++;
						}
                        if(filecount == filedownloaded){
		                	$("button[data-course-id="+downloadedID+"]").removeClass("btn-download").addClass("btn-start").text("Start");
		                	$("button[data-course-id="+downloadedID+"]").prop('disabled', false);
		                	$('.btn-download').prop('disabled', false);
                        }
					});

					saveCourseFiles();
				}
			})
			.fail(function( error, status ) {
			    error2(error);
			});
}

function downloadFile(filename,fileurl, i){
                    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function success(fileSystem){

                        fileSystem.root.getDirectory("theacademy",{create:true},function(d){
                            fileSystem.root.getFile("theacademy/"+downloadedID+"-"+i+"-"+filename, {create: true}, function(fileEntry){

                                var fileTransfer = new FileTransfer();
                                var uri = encodeURI(fileurl);
                                var filePath = fileSystem.root.toNativeURL()+"theacademy/"+downloadedID+"-"+i+"-"+filename;
                                window.localStorage.setItem("filesystem", fileSystem.root.toNativeURL());

                                fileTransfer.onprogress = function(progressEvent) {
									if (progressEvent.lengthComputable) {
										var fileperc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
										if ((fileperc) && (fileloads[i] != fileperc)) {
											fileloads[i] = fileperc;

											var totalperc=0;
											   for (var x=fileloads.length; x--;) {
											     totalperc += fileloads[x];
											   }
	//										console.log("totalperc: "+totalperc);
											var perc = Math.floor(totalperc / filecount)
	//										console.log("perc: "+perc);
											if (perc) {
												$("button[data-course-id="+downloadedID+"]").text(perc+"%");
												$("button[data-course-id="+downloadedID+"]").css("background", "linear-gradient(90deg, #0ab9a7 "+perc+"%,#2ce6f7 "+(perc+1)+"%)");
		                                        }
											}

									} else {

									}
								};
								
                                fileTransfer.download(uri, filePath, 
                                	function(entry) {
                                        console.log("download complete: " + filePath);
                                        filedownloaded++;
                                        if(filecount == filedownloaded){
                                        	$("button[data-course-id="+downloadedID+"]").removeClass("btn-download").addClass("btn-start").text("Start");
                                        	$("button[data-course-id="+downloadedID+"]").prop('disabled', false);
                                        	$('.btn-download').prop('disabled', false);
                                        }

                                    },
                                    function(error) {
                                        console.log("download error source 2 " + error.source);
                                        console.log("download error target 2 " + error.target);
                                        console.log("upload error code 2 " + error.code);
                                        $('.btn-download').prop('disabled', false);
                                    },
                                    false,
                                    {
                                        headers: {
                                            "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
                                        }
                                    }
                                );
                            }, 
                            function error(err){
                                console.log("err "+err.code);
                            });
                        },
                        function error(error){
                            alert("Erro "+error.code);
                        });

                    },
                    function error(error){
                        alert(error.code);
                    });
}

function saveCourseFiles(){
	//db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
	db.transaction(updateCourseFilesDB, error, successCreationDb);
}

function updateCourseFilesDB(t){
		t.executeSql('CREATE TABLE IF NOT EXISTS files (id, fileid, type, title, url, resources, downloaded)');
		$.each(downloadedCourse, function(i, data){
			var filePath = data.url;
			var filename = filePath.split("/").pop();
			if(data.type == 'link'){
				t.executeSql("INSERT OR IGNORE INTO files (id, fileid, type, title, url, resources, downloaded) VALUES ("+i+","+downloadedID+", '"+data.type+"', '"+data.title+"', '"+data.url+"', 'resources', 'YES') ", [], function(tx, results){
	                console.log('Returned ID: ' + results.insertId);
	            });
			}
			else{
				t.executeSql("INSERT OR IGNORE INTO files (id, fileid, type, title, url, resources, downloaded) VALUES ("+i+","+downloadedID+", '"+data.type+"', '"+data.title+"', 'theacademy/"+downloadedID+"-"+i+"-"+filename+"', 'resources', 'YES') ", [], function(tx, results){
	                console.log('Returned ID: ' + results.insertId);
	            });
			}
		    
		});

		runUpdateCourse();
}

function runUpdateCourse(){
	db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
	db.transaction(runUpdateCourseFilesDB, error);
}

function runUpdateCourseFilesDB(tx){
	var sql2 = "UPDATE courses SET downloaded='YES' WHERE id='"+downloadedID+"' ";
	tx.executeSql(sql2);
	console.log("ID Downloaded "+downloadedID);
	//db=null;
	//window.location = 'thingstodo.html';
}

function checkDevice(){
	var deviceType = (navigator.userAgent.match(/iPad/i))  == "iPad" ? "iPad" : (navigator.userAgent.match(/iPhone/i))  == "iPhone" ? "iPhone" : (navigator.userAgent.match(/Android/i)) == "Android" ? "Android" : (navigator.userAgent.match(/BlackBerry/i)) == "BlackBerry" ? "BlackBerry" : "null";

	return deviceType;
}

// Function delete databases
function deleteDB(){
	db = window.openDatabase('GEDB', '1.0', 'GEDB', 25242880);
	db.transaction(deleteDBs, error);
}

function deleteDBs(tx){
	var sql2 = "DROP TABLE IF EXISTS courses";
    var sql3 = "DROP TABLE IF EXISTS files";
    tx.executeSql(sql2);
    tx.executeSql(sql3);

    window.location = 'index.html';
}